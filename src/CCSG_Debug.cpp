/*******************************************************************************
 * Debugging tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#include "CCSG_Debug.h"
#include <Arduino.h>
#include "CCSG_Power.h"


#if defined(BOARD_ENVISENSE_FAMILY)
	#include "CCSG_FileSystem.h"
#endif

bool _debugLoggingToFileEnabled = true;
int _debugTraceNestingLevel = 0;

static char *_debug_morse[43] =
{
	(char*)"-----",
	(char*)".----",
	(char*)"..---",
	(char*)"...--",
	(char*)"....-",
	(char*)".....",
	(char*)"-....",
	(char*)"--...",
	(char*)"---..",
	(char*)"----.",
	(char*)"",
	(char*)"",
	(char*)"",
	(char*)"",
	(char*)"",
	(char*)"",
	(char*)"",
	(char*)".-",
	(char*)"-...",
	(char*)"-.-.",
	(char*)"-..",
	(char*)".",
	(char*)"..-.",
	(char*)"--.",
	(char*)"....",
	(char*)"..",
	(char*)".---",
	(char*)"-.-",
	(char*)".-..",
	(char*)"--",
	(char*)"-.",
	(char*)"---",
	(char*)".--.",
	(char*)"--.-",
	(char*)".-.",
	(char*)"...",
	(char*)"-",
	(char*)"..-",
	(char*)"...-",
	(char*)".--",
	(char*)"-..-",
	(char*)"-.--",
	(char*)"--.." };

void ccsg_debug_init(bool coldStart)
{
#if defined(BOARD_POWERDUE)  // **FIXME** not properly implemented for PowerDue
	pd_rgb_led(PD_OFF);
#endif

#if defined(BOARD_ENVISENSE_FAMILY)
#if defined(INCLUDE_FILE_FUNCTIONS)
	ccsg_filesystem(ACTION_ENABLE);                   // For debug logging
#endif
	if (coldStart)
	{
#if defined(INCLUDE_FILE_FUNCTIONS)
		ccsg_debug_rotate_logs();
#endif
		ccsg_power_set_state(DEVICE_DEBUG, ACTION_ENABLE);   // **FIXME** not very general -- should be bundled with CCSG_DEBUG_BEGIN
	}
#endif
	CCSG_DEBUG_BEGIN(DEBUG_BAUD);
#if defined(BOARD_ENVISENSE_FAMILY) && defined(WAIT_FOR_SERIAL_MONITOR)
	CCSG_DEBUG_WAIT();
#endif
}


void ccsg_debug_print_stack_pointer(bool save_p)
{
	static void *base;
	void *p = NULL;
	void *sp;

	sp = &p;
	if (save_p)
		base = sp;
	CCSG_DEBUG_PRINTF("[debug   ] Stack pointer: %p; depth %d\r\n", (void* )&p, (int)base - (int)sp);
}


int ccsg_debug_grow_stack(int n)
{
	int m = n + 1;
	DEBUG_SERIAL.println(n);
	int j = ccsg_debug_grow_stack(m);
	return j+n+random(100);
}


void ccsg_debug_led_green_on()
{
	digitalWrite(LED_GREEN, LED_ON);
}


void ccsg_debug_blink(int msec)
{
	digitalWrite(LED_RED, LED_ON);
	delay(msec);
	digitalWrite(LED_RED, LED_OFF);
	delay(DOT_MSEC);
}


// **WARNING** long messages can cause the watchdog timer to engage.  Keep it brief.
void ccsg_debug_morse_led(const char string[])
{
	int stringLength = strlen(string);
	for (int i = 0; i < stringLength; i++)
	{
		// begin letter
		if (string[i] == ' ')
		{
			delay(WORD_SPACE_MSEC);
			// ccsg_debug_print("    ");
			continue;
		}
		char character = toupper(string[i]);
		if ((character >= '0') && (character <= 'Z'))
		{
			char *pattern = _debug_morse[character - '0'];
			int patternLength = strlen(pattern);
			for (int j = 0; j < patternLength; j++)
			{
				digitalWrite(LED_RED, LED_ON);
				if (pattern[j] == '.')
				{
					delay(DOT_MSEC);
					// ccsg_debug_print(".");
				}
				else
				{
					delay(DASH_MSEC);
					// ccsg_debug_print("-");
				}

				digitalWrite(LED_RED, LED_OFF);
				// For all dot-dashes except the last, leave an inter-dot-dash space
				if (j < patternLength - 1)
				{
					delay(INTER_DOT_DASH_MSEC);
					// ccsg_debug_print(" ");
				}
			}

		} // end letter
		  // For all letters except the last, leave an inter-letter space
		if (i < stringLength - 1)
		{
			delay(LETTER_SPACE_MSEC);
			// ccsg_debug_print("/");
		}
	}
	delay(WORD_SPACE_MSEC);
	// ccsg_debug_print("    ");
}




// freeMemory() https://github.com/mpflaga/Arduino-MemoryFree
extern "C" char* sbrk(int incr);

int ccsg_debug_measure_free_memory()
{
	char top;
	return &top - reinterpret_cast<char*>(sbrk(0));
}
// end freeMemory


void ccsg_debug_log_free_memory()
{
	static int fm = ccsg_debug_measure_free_memory();
	int new_fm;

	new_fm = ccsg_debug_measure_free_memory();
	CCSG_DEBUG_PRINTF("[debug   ] Free memory: %d\r\nReduction in Min Free Memory: %d\r\n",
			new_fm, fm - new_fm);

	fm = min(new_fm, fm);
}


void ccsg_debug_print_memory(uint8_t buffer[], int length)
{
	for (int i = 0; i < length; i++)
	{
		if ((i != 0) & (i % 16 == 0))
			CCSG_DEBUG_PRINTLN(F(""));
		if (i % 16 == 0)
		{
			CCSG_DEBUG_PRINTF("[debug   ] %04X: ", i);
		}
		CCSG_DEBUG_PRINTF("%02X", buffer[i]);
		if (i % 4 == 3)
			CCSG_DEBUG_PRINT("   ");
		else
			CCSG_DEBUG_PRINT(" ");
	}
	CCSG_DEBUG_PRINTLN(F(""));
}

void ccsg_debug_log(const __FlashStringHelper *ifsh)
{
	ccsg_debug_log(reinterpret_cast<const char*>(ifsh));
}


void ccsg_debug_print_pio_configuration()
{
#if defined(BOARD_ENVISENSE_FAMILY)
	int pio_index;
	int i;
	Pio *pios[] =
	{ PIOA, PIOB, PIOC, PIOD };
	int reg_len[] =
	{ 30, 32, 31, 11 };
	Pio *pio;
	char *pio_names[] =
	{ (char*)"PA", (char*)"PB", (char*)"PC", (char*)"PD" };
	char *pio_name;
	bool pullup_p;
	uint32_t osr; // output status register: =1: output enabled   =0: pure input
	uint32_t sodr;    // for outputs, determines drive state (0 or 1)
	uint32_t pusr;    // pull-up status:  =1: pull-up DISABLED
	uint32_t psr;	  // PIO controls the pin:  =1: PIO controls

	for (pio_index = 0; (unsigned)pio_index < (sizeof(pio_names) / sizeof(pio_names[0]));
			pio_index++)
	{
		pio = pios[pio_index];
		pio_name = pio_names[pio_index];
		osr = pio->PIO_OSR;
		sodr = pio->PIO_ODSR;
		pusr = pio->PIO_PUSR;
		psr = pio->PIO_PSR;
		for (i = 0; i < reg_len[pio_index]; i++)
		{
			pullup_p = (bool) (0x01 & (pusr >> i));
			if (0x01 & (psr >> i))
			{
				if (0x01 & (osr >> i))
				{
					CCSG_DEBUG_PRINTF(
							"[debug   ] PIO_%s%02d is configured as output_%u %s a pull-up\r\n",
							pio_name, i, (unsigned)(0x01 & (sodr >> i)),
							pullup_p ? "without" : "with   ");
				}
				else
				{
					CCSG_DEBUG_PRINTF(
							"[debug   ] PIO_%s%02d is configured as input    %s a pull-up\r\n",
							pio_name, i, pullup_p ? "without" : "with   ");
				}
			}
			else
				CCSG_DEBUG_PRINTF(
						"[debug   ] PIO_%s%02d is peripheral-controlled  %s a pull-up\r\n",
						pio_name, i, pullup_p ? "without" : "with   ");
		}
	}
#endif
}


#if defined(BOARD_ENVISENSE_FAMILY) && defined(INCLUDE_FILE_FUNCTIONS)
// Assumes ccsg_filesystem(ACTION_ENABLE) has been run by pepperwood.cpp once and for all
void ccsg_debug_log(const char *string)
{
	FRESULT file_result;
	FIL file;

	if (_debugLoggingToFileEnabled)
	{
		if (!digitalRead(SD_DETECT)) // SD_DETECT is LOW when the SD subsystem is powered up and a card is inserted
		{
			file_result = ccsg_filesystem_open_append(&file, SYSLOG_FILE_PATH);
			if (file_result == FR_OK)
				f_printf(&file, string);
			else
			{
				DEBUG_SERIAL.print("[debug   ] *** ERROR: can't open log file: ");
				DEBUG_SERIAL.print(ccsg_filesystem_string_for_fatfs_error(file_result));
				DEBUG_SERIAL.println("");
				_debugLoggingToFileEnabled = false;
				ccsg_debug_morse_led((char*)"D0");
			}
			f_close(&file);
		}
		else
		{
			_debugLoggingToFileEnabled = false;
			ccsg_debug_morse_led((char*)"SD");
		}
	}
}


// Assumes ccsg_filesystem(ACTION_ENABLE) has been run by pepperwood.cpp once and for all
void ccsg_debug_rotate_logs()
{
	// FRESULT file_result;
	// FIL file;

	if (_debugLoggingToFileEnabled)
	{
		if (!digitalRead(SD_DETECT)) // SD_DETECT is LOW when the SD subsystem is powered up and a card is inserted
		{

			FILINFO fno;
			DIR dir;
			FRESULT res;
			uint16_t i = 0;  // numeric part of filename restricted to five digits
			uint32_t max = 0; // could be 99999 -- it comes from the file names
			char logName[13]; // max length of FATFS filename is 13 (8 byte file, 3 byte extension)
			char logPath[32];
			// Make directory for logging files if not present
			if (f_stat(SYSLOG_DIRECTORY, &fno) == FR_NO_FILE) {
				f_mkdir(SYSLOG_DIRECTORY);
			}

			// Parse directory to find what previous file should renamed to
			res = f_opendir(&dir, SYSLOG_DIRECTORY);
			if (res == FR_OK) {
				for ( ;; ) {
					res = f_readdir(&dir, &fno);
					if (res != FR_OK || fno.fname[0] == 0) {
						// Break if reading dir return error or filename is null, i.e., end of dir
						break;
					}

					if (fno.fattrib & AM_DIR) {
						// This is also a directory; this should not happen
						//    Is there anything worth doing here?

					}
					else {
						// Retrieve number at end of the filename; pattern MUST be all uppercased as FATFS has all file names uppercase

						// The compiler checks the size of the %d field in the pattern.
						sscanf(fno.fname, SYSLOG_BACKUP_PATTERN, (int*)&i);  // five digits max 0..65535

						if ( i > max) {
							max = i;
						}
					}
				}

				// Format the path that the log file should be renamed to, including the directory

				if (max < 65535)
					max++;
				else
					max = 65535;  // saturating... this will cause the latest log to be over-written **FIXME**

				sprintf(logName, SYSLOG_BACKUP_PATTERN, (int)max);
				sprintf(logPath, "%s/%s", SYSLOG_DIRECTORY, logName);

				// Rename the log to be the backup log
				f_rename(SYSLOG_FILE_PATH, logPath);
			}


		}
	}
}





#else

void ccsg_debug_log(const char *string){}
void ccsg_debug_rotate_logs(){}
void ccsg_debug_log_location(float lat, float lon, float alt){}

#endif

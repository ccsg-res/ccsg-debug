//
// Poor man's realization of printf -- that pushes bytes to SerialUSB
//
// Usage:
//   in your C file:
//     #include "../../../PowerDue_Debug/myprintf.h"  (may have to modify the path)
//     myPrintf(...);
//
// 2019-0403 Bob Iannucci
//

#ifdef __cplusplus
extern "C"
#endif
void myprintf(const char *format, ...);

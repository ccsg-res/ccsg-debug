/*******************************************************************************
 * Debugging tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_DEBUG_H__
#define __CCSG_DEBUG_H__

#include "variant.h"
#include "CCSG_BoardConfig.h"

#define DOT_MSEC            200
#define DASH_MSEC           3 * DOT_MSEC
#define INTER_DOT_DASH_MSEC 150
#define LETTER_SPACE_MSEC   5 * DOT_MSEC
#define WORD_SPACE_MSEC     9 * DOT_MSEC

extern int _debugTraceNestingLevel;


#define SHOW_FREE_MEMORY_CHANGE(function)                                          \
	do{                                                                       	   \
		CCSG_DEBUG_PRINTLN(F("vvvvvvvvvvvvvvv Before " #function));                \
		int __debug_before = ccsg_debug_freeMemory();                              \
		ccsg_debug_log_free_memory();                                              \
		function ;                                                                 \
		int __debug_after = ccsg_debug_freeMemory();                               \
        ccsg_debug_log_free_memory();                                              \
		CCSG_DEBUG_PRINT(F("^^^^^^^^^^^^^^^ After " #function "... reduction in freeMemory() is "));   \
		CCSG_DEBUG_PRINTLN(__debug_before - __debug_after);                        \
	}while(0)


#if defined(INCLUDE_DEBUG_FUNCTIONS)
  // printf on the Arduino by Michael McElligott
  // https://forum.arduino.cc/index.php?topic=370464.0
  #define _DEBUG_PRINTF_BUFFER_LENGTH_		200
  static char _debug_lib_pf_buffer_[_DEBUG_PRINTF_BUFFER_LENGTH_] __attribute__((unused));
  #define CCSG_DEBUG_PRINTF(a,...)                                              \
    do{                                                                         \
    snprintf(_debug_lib_pf_buffer_, sizeof(_debug_lib_pf_buffer_), a, ##__VA_ARGS__);  \
    DEBUG_SERIAL.print(_debug_lib_pf_buffer_);                                  \
    ccsg_debug_log(_debug_lib_pf_buffer_);                                      \
    DEBUG_SERIAL.flush();													    \
    }while(0)
  #define CCSG_DEBUG_BEGIN(baud) DEBUG_SERIAL.begin(baud)
  #define CCSG_DEBUG_WAIT() { while (!DEBUG_SERIAL){} }

  #define CCSG_DEBUG_PRINT(...) 												\
    do{																			\
    	DEBUG_SERIAL.print(__VA_ARGS__);										\
        ccsg_debug_log(__VA_ARGS__);                                          	\
    	DEBUG_SERIAL.flush();													\
    }while(0)

  #define CCSG_DEBUG_PRINTLN(...) 												\
    do{																			\
  	  	  DEBUG_SERIAL.println(__VA_ARGS__);									\
          ccsg_debug_log(__VA_ARGS__);                                        	\
          ccsg_debug_log("\r\n");                                             	\
  	  	  DEBUG_SERIAL.flush();													\
    }while(0)

  #define CCSG_DEBUG_TRACE_ENTER() { \
  	CCSG_DEBUG_PRINTF("[trace   ] %.*s+%s\r\n", _debugTraceNestingLevel, "  ", __FUNCTION__); \
  	_debugTraceNestingLevel++; \
  }

  #define CCSG_DEBUG_TRACE_EXIT() { \
	_debugTraceNestingLevel--; \
  	CCSG_DEBUG_PRINTF("[trace   ] %.*s-%s\r\n", _debugTraceNestingLevel, "  ", __FUNCTION__); \
  }

#else
  #define CCSG_DEBUG_BEGIN(baud) {}
  #define CCSG_DEBUG_WAIT() {}
  #define CCSG_DEBUG_PRINT(...) {}
  #define CCSG_DEBUG_PRINTLN(...) {}
  #define CCSG_DEBUG_PRINTF(a,...) {}
  #define CCSG_DEBUG_TRACE_ENTER() {}
  #define CCSG_DEBUG_TRACE_EXIT() {}
#endif

void ccsg_debug_led_green_on();
void ccsg_debug_morse_led(const char string[]);
void ccsg_debug_init(bool coldStart);
int  ccsg_debug_measure_free_memory();
void ccsg_debug_log_free_memory();
void ccsg_debug_print_memory(uint8_t buffer[], int length);
void ccsg_debug_print_pio_configuration();
void ccsg_debug_log(const char* string);
void ccsg_debug_log(const __FlashStringHelper *ifsh);  // this supports F("...")
void ccsg_debug_rotate_logs();
int  ccsg_debug_grow_stack(int n);
void ccsg_debug_print_stack_pointer(bool save_p);

#endif  // __CCSG_DEBUG_H__

# CCSG Debug Library

This library offers functions for formatting and displaying messages, examining stack depth, and more used by the CCSG EnviSense family of boards.
